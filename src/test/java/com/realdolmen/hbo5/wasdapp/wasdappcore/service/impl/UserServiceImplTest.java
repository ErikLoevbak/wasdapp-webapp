package com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.UserRepo;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {


    @Mock
    private UserRepo userRepo;

    @InjectMocks
    private UserServiceImpl userService;

    private UserDTO userDTO;

    @Before
    public void init() {
        userDTO = new UserDTO();
    }

    @Captor
    private ArgumentCaptor<String> captor;
    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;
    @Captor
    private ArgumentCaptor<UserDTO> dtoArgumentCaptor;

    @After
    public void tearDown() {
        verifyNoMoreInteractions(userRepo);
    }

    @Test
    public void saveUserDtoTestFail() {
        //dto initialiseren
        userDTO.setUserName("jelter");

        // repo functie uitvoeren met verwachte resultaat
        when(userRepo.findByUserNameEquals("jelter")).thenReturn(new User());

        // kortere versie van assertEqual en je gaat de parameter meegeven
        assertFalse(userService.save(userDTO));

        // Deze captor gaat de string in de parameter opvangen
        verify(userRepo).findByUserNameEquals(captor.capture());

        //Hier neem je de captor value op en gaat het vergelijken met de string
        assertThat(captor.getValue(), is("jelter"));
    }

    @Test
    public void saveUserDtoTestSucces() {
        userDTO.setUserName("false");
        userDTO.setAdmin(false);
        User user = new User();
        user.setUserName("false");

        when(userRepo.findByUserNameEquals(any())).thenReturn(null);

        boolean exist = userService.save(userDTO);

        assertTrue(exist);

        verify(userRepo).findByUserNameEquals("false");
        verify(userRepo).save(userArgumentCaptor.capture());
        verifyNoMoreInteractions(userRepo);
        assertThat(userArgumentCaptor.getValue(),samePropertyValuesAs(user));
    }

    @Test
    public void checkUserLoginTestUsername() {
        userDTO.setUserName("A");
        when(userRepo.findByUserNameEquals("A")).thenReturn(null);

        UserDTO outputDTO = userService.checkUserLogin(userDTO);

        assertEquals("false", outputDTO.getUserName());

        verify(userRepo).findByUserNameEquals("A");
        verifyNoMoreInteractions(userRepo);
    }
    @Test
    public void checkUserLoginTestWrongpass(){
        userDTO.setUserName("A");
        userDTO.setPassword("A");
        User user = new User();
        user.setUserName("A");
        user.setPassword("B");
        when(userRepo.findByUserNameEquals("A")).thenReturn(user);

        UserDTO outputDTO = userService.checkUserLogin(userDTO);

        assertEquals("false", outputDTO.getUserName());

        verify(userRepo).findByUserNameEquals("A");
        verifyNoMoreInteractions(userRepo);

    }
    @Test
    public  void checkUserLoginTestSucess(){
        userDTO.setUserName("B");
        userDTO.setPassword("A");
        User user = new User();
        user.setUserName("A");
        user.setPassword("A");
        when(userRepo.findByUserNameEquals("B")).thenReturn(user);

        UserDTO outputDTO = userService.checkUserLogin(userDTO);

        assertEquals("A", outputDTO.getUserName());

        verify(userRepo).findByUserNameEquals("B");
        verifyNoMoreInteractions(userRepo);


    }


}
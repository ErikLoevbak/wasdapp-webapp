package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.WasdappEntryRepository;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.WasdappServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WasdappServiceImplTest {


    @Mock
    private WasdappEntryRepository repositoryMock;
    private WasdappServiceImpl service;


    @Before
    public void setUp() {
        service = new WasdappServiceImpl(repositoryMock);
    }

    @Test
    public void findByNameContains() {
        WasdappEntry entry1 = wasdappEntryFor("A", "A");
        WasdappEntry entry2 = wasdappEntryFor("B", "B");
        List repoResponse = Arrays.asList(entry1, entry2);
        when(repositoryMock.findByNameContaining("koffie")).thenReturn(repoResponse);
        List<WasdappEntryResponse> serviceResponse = service.findByNameContains("koffie");
        assertThat(serviceResponse.size()).isEqualTo(repoResponse.size());
        assertThat(serviceResponse.get(1) != repoResponse.get(1));
        assertThat(serviceResponse.get(1).getName() == serviceResponse.get(1).getName());
        verify(repositoryMock, times(1)).findByNameContaining("koffie");
        verifyNoMoreInteractions(repositoryMock);

    }

    private WasdappEntry wasdappEntryFor(String name, String description) {
        WasdappEntry wasdappEntry = new WasdappEntry();
        wasdappEntry.setName(name);
        wasdappEntry.setId(1L);
        wasdappEntry.setDescription(description);
        wasdappEntry.setStreet("street");
        wasdappEntry.setNumber("number");
        wasdappEntry.setCity("city");
        wasdappEntry.setLat(2.02);
        wasdappEntry.setLon(3.03);
        return wasdappEntry;
    }
}
package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.service.CsvParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UploadControllerTest {

    @Mock
    private CsvParser csvParser;

    @Mock
    private MultipartFile file;

    @InjectMocks
    private UploadController uploadController;

    @Test
    public void singleFileUploadTestInvocations() throws IOException {
        String s = uploadController.singleFileUpload(file);

        assertEquals("redirect:/homepage", s);

        verify(csvParser).importCsv(any());
        verify(file).getInputStream();
        verifyNoMoreInteractions(file, csvParser);
    }

    @Test
    public void singleFileUploadTestCatchesException() throws IOException {
        when(file.getInputStream()).thenThrow(IOException.class);
        String s = uploadController.singleFileUpload(file);

        assertEquals("redirect:/homepage", s);
        verifyNoMoreInteractions(csvParser);
    }
}

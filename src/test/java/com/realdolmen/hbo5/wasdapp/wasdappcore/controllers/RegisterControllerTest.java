package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RegisterControllerTest {

    @Mock
    private CurrentUser currentUser;

    @Mock
    private UserService userService;

    @Mock
    private Model model;

    @InjectMocks
    private RegisterController registerController;

    private UserDTO userDTO;

    @Before
    public void init(){
        userDTO = new UserDTO();
    }

    @Test
    public void wasDappRegisterPageTestIndexRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(null);
        String s = registerController.wasDappRegisterPage(new UserDTO(), model);

        assertEquals("redirect:/index", s);

        verify(currentUser).getCurrentUser();
        verifyNoMoreInteractions(currentUser, model, userService);
    }

    @Test
    public void wasDappRegisterPageTestHomepageRedirect(){
        userDTO.setAdmin(false);
        when(currentUser.getCurrentUser()).thenReturn(userDTO);
        String s = registerController.wasDappRegisterPage(new UserDTO(), model);

        assertEquals("redirect:/homepage", s);

        verify(currentUser, times(2)).getCurrentUser();
        verifyNoMoreInteractions(currentUser, model, userService);
    }

    @Test
    public void wasDappRegisterPageCorrectInvocations(){
        userDTO.setAdmin(true);
        when(currentUser.getCurrentUser()).thenReturn(userDTO);
        String s = registerController.wasDappRegisterPage(new UserDTO(), model);

        assertEquals("register.html", s);

        verify(currentUser, times(3)).getCurrentUser();
        verify(model).addAttribute("exist", "false");
        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
       //verifyNoMoreInteractions(model, currentUser, userService);
    }

    @Test
    public void registerUserTestSuccess(){
        when(currentUser.getCurrentUser()).thenReturn(userDTO);
        when(userService.save(userDTO)).thenReturn(true);
        String s = registerController.registerUser(userDTO, model);

        assertEquals("redirect:/homepage", s);

        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute(eq("user"), any());
        verify(userService).save(userDTO);
        verify(currentUser).getCurrentUser();
       verifyNoMoreInteractions(model, currentUser, userService);
    }

    @Test
    public void registerUserTestFailure(){
        when(currentUser.getCurrentUser()).thenReturn(userDTO);
        when(userService.save(userDTO)).thenReturn(false);
        String s = registerController.registerUser(userDTO, model);

        assertEquals("register.html", s);

        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute("exist", "true");
        verify(model).addAttribute(eq("user"), any());
        verify(userService).save(userDTO);
        verify(currentUser).getCurrentUser();
        verifyNoMoreInteractions(model, currentUser, userService);
    }

}

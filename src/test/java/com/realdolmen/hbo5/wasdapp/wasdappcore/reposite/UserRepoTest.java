package com.realdolmen.hbo5.wasdapp.wasdappcore.domain;

import com.google.common.truth.Truth;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.UserRepo;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.WasdappEntryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.AssertTrue;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepoTest {
    @Autowired
    private UserRepo userRepo;

    @Test
    public void name() {
        Truth.assertThat(userRepo.findById(1L).isPresent()).isTrue();
    }


    @Test
    public void saveTestSaved() {
        User user = new User();
        List<User> userList = userRepo.findAll();
        User userBeforeInsert = userRepo.findByUserNameEquals("A");
        user.setUserName("A");
        userRepo.save(user);
        Truth.assertThat(userRepo.findAll().size() == userList.size() + 1);
        assertNotNull(userRepo.findByUserNameEquals("A"));
        assertNull(userBeforeInsert);

    }
}

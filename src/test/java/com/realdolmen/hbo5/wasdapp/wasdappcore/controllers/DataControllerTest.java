package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DataControllerTest {


    @Mock
    private WasdappService wasdappService;

    @Mock
    private CurrentUser currentUser;

    @Mock
    private Model model;

    @InjectMocks
    private DataController dataController;

    @Captor
    private ArgumentCaptor<Long> captor;

    private List<Long> idList;

    @Before
    public void init(){
        idList = new ArrayList<>();
        idList.add(1L);
        idList.add(2L);
        idList.add(3L);
        idList.add(-1L);
    }


    @Test
    public void navigateToHomePageTestRedirects() {
        when(currentUser.getCurrentUser()).thenReturn(null);

        String s = dataController.navigateToHomePage(model);

        assertEquals("redirect:/index", s);


        verify(model).addAttribute(eq("index"), any());
        verify(wasdappService).findAll();
        verify(currentUser).getCurrentUser();
        verifyNoMoreInteractions(model, wasdappService, currentUser);

    }

    @Test
    public void navigateToHomePageTestLandsOnHomePage(){
        when(currentUser.getCurrentUser()).thenReturn(new UserDTO());
        String s = dataController.navigateToHomePage(model);

        assertEquals("homepage.html", s);

        verify(model).addAttribute(eq("index"), any());
        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute(eq("user"), any());
        verify(wasdappService).findAll();
        verify(currentUser, times(2)).getCurrentUser();
      verifyNoMoreInteractions(model, wasdappService, currentUser);
    }


    @Test
    public void deleteMultipleTestIndexRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(null);

        String s = dataController.deleteMultiple(idList, model);

        assertEquals("redirect:/index", s);

        verify(currentUser).getCurrentUser();
        verifyNoMoreInteractions(currentUser, model, wasdappService);
    }

    @Test
    public void deleteMultipleTestSuccess(){
        when(currentUser.getCurrentUser()).thenReturn(new UserDTO());

        String s = dataController.deleteMultiple(idList, model);

        assertEquals("redirect:/homepage", s);

        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute(eq("user"), any());
        verify(currentUser, times(2)).getCurrentUser();
        verify(wasdappService, times(3)).deleteById(captor.capture());

        MatcherAssert.assertThat(captor.getAllValues().size(), is(3));
        MatcherAssert.assertThat(captor.getAllValues().get(0), samePropertyValuesAs(1L));
        MatcherAssert.assertThat(captor.getAllValues().get(1), samePropertyValuesAs(2L));
        MatcherAssert.assertThat(captor.getAllValues().get(2), samePropertyValuesAs(3L));

       verifyNoMoreInteractions(model, currentUser, wasdappService);
    }

}

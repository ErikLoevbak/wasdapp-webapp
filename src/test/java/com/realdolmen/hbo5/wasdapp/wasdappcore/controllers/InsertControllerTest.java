package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;


import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class InsertControllerTest {

    @Mock
    private WasdappService wasdappService;

    @Mock
    private CurrentUser currentUser;

    @Mock
    private Model model;

    @InjectMocks
    private InsertController insertController;

    @Test
    public void wasDappInsertPageTestNoRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(new UserDTO());
        String s = insertController.wasDappInsertPage(new WasdappEntry(), model);

        assertEquals("insert.html", s);

        verify(currentUser, times(2)).getCurrentUser();
        verify(model).addAttribute(eq("buttonText"), any());
        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute(eq("user"), any());

        verifyNoMoreInteractions(model, currentUser);
    }

    @Test
    public void wasDappInsertPageTestRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(null);
        String s = insertController.wasDappInsertPage(new WasdappEntry(), model);

        assertEquals("redirect:/index", s);

        verify(currentUser).getCurrentUser();
        verify(model).addAttribute(eq("buttonText"), any());
        verifyNoMoreInteractions(model, currentUser);
    }

    @Test
    public void wasDappEditPageTestNoRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(new UserDTO());
        String s = insertController.wasDappEditPage(1L, model);

        assertEquals("insert.html", s);

        verify(wasdappService).getOne(1L);
        verify(currentUser, times(2)).getCurrentUser();
        verify(model).addAttribute(eq("buttonText"), any());
        verify(model).addAttribute(eq("wasdappEntry"), any());
        verify(model).addAttribute(eq("currentUser"), any());
        verify(model).addAttribute(eq("LoggedIn"), any());
        verify(model).addAttribute(eq("user"), any());
        verifyNoMoreInteractions(model, currentUser, wasdappService);
    }

    @Test
    public void wasDappEditPageTestRedirect(){
        when(currentUser.getCurrentUser()).thenReturn(null);
        String s = insertController.wasDappEditPage(1L, model);

        assertEquals("redirect:/index", s);

        verify(wasdappService).getOne(1L);
        verify(currentUser).getCurrentUser();
        verify(model).addAttribute(eq("wasdappEntry"), any());
        verify(model).addAttribute(eq("buttonText"), any());
        verifyNoMoreInteractions(model, currentUser, wasdappService);
    }

    @Test
    public void wasDappEntryAddTestSaved(){
        WasdappEntry wasdappEntry = new WasdappEntry();
        String s = insertController.wasdappEntryAdd(wasdappEntry);

        assertEquals("redirect:/homepage", s);

        verify(wasdappService).save(wasdappEntry);
        verifyNoMoreInteractions(wasdappService);
    }
}

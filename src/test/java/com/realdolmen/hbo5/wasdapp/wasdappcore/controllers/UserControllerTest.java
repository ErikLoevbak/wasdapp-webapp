package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;



@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserControllerTest {


    @Mock
    private UserService userService;

    @Mock
    private CurrentUser currentUser;

    @Mock
    private Model model;

    @InjectMocks
    private UserController userController;

    private UserDTO userDTO;

    @Before
    public void init(){
        userDTO = new UserDTO();
    }

    @Test
    public void indexRedirectTestReturnsRedirect(){
        String s = userController.indexRedirect();
        assertEquals("redirect:/index", s);
    }

    @Test
    public void loginPageTestInvocations(){

        userDTO.setAdmin(true);
        String s = userController.loginPage(userDTO, model);

        assertEquals("index.html", s);

        verify(model).addAttribute(eq("userDTO"), any());
        verifyNoMoreInteractions(model, currentUser);

    }


    @Test
    public void loginUserTestFailure(){
        userDTO.setUserName("false");
        when(userService.checkUserLogin(userDTO)).thenReturn(userDTO);
        String s = userController.loginUser(userDTO);

        assertEquals("index.html", s);

        verify(userService).checkUserLogin(userDTO);
        verifyNoMoreInteractions(userService, model, currentUser);
    }

    @Test
    public void loginUserTestSuccess(){
        userDTO.setUserName("A");
        when(userService.checkUserLogin(userDTO)).thenReturn(userDTO);
        String s = userController.loginUser(userDTO);

        assertEquals("redirect:/homepage", s);

        verify(userService).checkUserLogin(userDTO);
        verify(currentUser).setCurrentUser(userDTO);
        verifyNoMoreInteractions(currentUser, model, userService);

    }

    @Test
    public void logoutUserTest(){
        String s = userController.logoutUser();

        assertEquals("redirect:/index", s);

        verify(currentUser).setCurrentUser(null);
        verifyNoMoreInteractions(currentUser, model, userService);
    }
}


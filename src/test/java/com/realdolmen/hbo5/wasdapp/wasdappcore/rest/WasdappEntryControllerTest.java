package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.CsvParser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WasdappEntryControllerTest {
    private WasdappEntryController wasdappEntryController;
    @Mock
    private WasdappService serviceMock;

    @Mock
    private MultipartFile file;

    @Mock
    private CsvParser csvParser;


    @Before
    public void setUp() {
        wasdappEntryController = new WasdappEntryController(serviceMock, csvParser);
    }

    @Test
    public void findByNameLike() {
        WasdappEntryResponse entry1 = wasdappEntryResponseFor("koffiemachien", "Koffiemachien met deca");
        WasdappEntryResponse entry2 = wasdappEntryResponseFor("koffiemachien", "Koffiemachien met goed gerief");


        when(serviceMock.findByNameContains("koffie")).thenReturn(Arrays.asList(entry1, entry2));


        List<WasdappEntryResponse> result = wasdappEntryController.searchByNameLike("koffie");

        assertThat(result).hasSize(2);
        assertThat(result.get(0).getName()).isEqualTo("koffiemachien");
        assertThat(result.get(0).getDescription()).isEqualTo("Koffiemachien met deca");
        assertThat(result.get(1).getName()).isEqualTo("koffiemachien");
        assertThat(result.get(1).getDescription()).isEqualTo("Koffiemachien met goed gerief");

    }

    @Test
    public void insert() {
        WasdappEntry successEntry = wasdappEntryFor("success", "this should succeed");
        WasdappEntry failureEntry = wasdappEntryFor("failure", "this should fail");
        when(serviceMock.save(successEntry)).thenReturn(successEntry);
        when(serviceMock.save(failureEntry)).thenReturn(wasdappEntryFor("asdfasdf", "asdfasdf"));

        Boolean success = wasdappEntryController.insert(successEntry);

        verify(serviceMock, times(1)).save(successEntry);
        assertThat(success == true);
        verifyNoMoreInteractions(serviceMock);
        Boolean failure = wasdappEntryController.insert(failureEntry);
        verify(serviceMock, times(1)).save(failureEntry);
        assertThat(success == false);
        verifyNoMoreInteractions(serviceMock);

    }

    @Test
    public void findAll() {
        WasdappEntryResponse response1 = wasdappEntryResponseFor("A", "A");
        WasdappEntryResponse response2 = wasdappEntryResponseFor("B", "B");

        when(serviceMock.findAll()).thenReturn(Arrays.asList(response1, response2));

        List<WasdappEntryResponse> responseList = wasdappEntryController.findAll();

        verify(serviceMock, times(1)).findAll();
        verifyNoMoreInteractions(serviceMock);
        assertThat(responseList.size() == 2);
        assertThat(responseList.get(1).getName() == "B");

    }

    @Test
    public void findOne() {
        WasdappEntryResponse response1 = wasdappEntryResponseFor("A", "A");
        when(serviceMock.getOne(1L)).thenReturn(response1);
        WasdappEntryResponse response = wasdappEntryController.findOne(1L);

        verify(serviceMock, times(1)).getOne(1L);
        verifyNoMoreInteractions(serviceMock);
        assertThat(response.getName() == "A");
    }

    @Test
    public void delete() {
        wasdappEntryController.deleteEntry(1L);
        verify(serviceMock, times(1)).deleteById(1L);
        verifyNoMoreInteractions(serviceMock);
    }

    private WasdappEntryResponse wasdappEntryResponseFor(String name, String description) {
        WasdappEntryResponse wasdappEntry = WasdappEntryResponse.builder().setId(1L).setName(name).setDescription(description)
                .setStreet("street")
                .setNumber("number")
                .setCity("city")
                .setLat(2.02)
                .setLon(3.03)
                .build();
        return wasdappEntry;
    }

    private WasdappEntry wasdappEntryFor(String name, String description) {
        WasdappEntry wasdappEntry = new WasdappEntry();
        wasdappEntry.setName(name);
        wasdappEntry.setId(1L);
        wasdappEntry.setDescription(description);
        wasdappEntry.setStreet("street");
        wasdappEntry.setNumber("number");
        wasdappEntry.setCity("city");
        wasdappEntry.setLat(2.02);
        wasdappEntry.setLon(3.03);
        return wasdappEntry;
    }

}
create table wasdapp_entry (
    id int8 auto_increment primary key not null,
    name varchar(255) not null,
    description varchar(2048),
    street varchar(96),
    number varchar(10),
    city varchar(86),
    lat double precision,
    lon double precision,
    postcode varchar(4),
    wiki varchar(255),
    web varchar(255),
    tel varchar(15),
    email varchar(255),
    person varchar(255)
);


insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('Mixer','Mixer voor smoothies','Rue lieutenant dse',4,'Brussels',14.000,154.00,1000,'wikiLink','www.Mixer.be', '0471689452','info@mixer.be','Dirk');
insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('Boormachine','Gaatjes in  muur ','Rue lieutenant dse',4,'Antwerp',12.000,155.00,2000,'wikiLink','www.Boormachine.be', '0471689452','info@Boormachine.be','Mark');
insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('Snackmachine','Machine vol suiker','Rue lieutenant dse',5,'Brussels',17.000,15.00,1000,'wikiLink','www.Snackmachine.be', '0471689452','info@Snackmachine.be','Micheal');
insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('Broodjes maker','Glutenvrij','Rue lieutenant dse',6,'Gent',17.000,15.00,9000,'wikiLink','www.BroodjesMaker.be', '0471689452','info@Broodjesmaker.be','Frank');
insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('koelkast','Lekker fris','Rue lieutenant dse',7,'Leuven',18.000,20.00,3000,'wikiLink','www.koelkast.be', '0471689452','info@koelkast.be', 'Jenny');
insert into wasdapp_entry(name,description,street,number,city,lat,lon,postcode,wiki,web,tel,email,person) values ('Kast','Opbergruimte','Rue lieutenant dse',8,'Vilvoord',18.000,20.00,1800,'wikiLink','www.Kast.be', '0471689452','info@kast.be','Arnold');


create table users(
id int8 auto_increment primary key not null,
username varchar(255) not null,
password varchar(2048),
is_admin BOOLEAN
);

insert into users ( username, password, is_admin) values ('Sedric', 'sedric',true);
insert into users ( username, password, is_admin) values ('michael', 'michael',true);
insert into users ( username, password, is_admin) values ('erik', 'erik',false);
insert into users ( username, password, is_admin) values ('silvio', 'sivlio',false);
insert into users ( username, password, is_admin) values ('jelter', 'jelter',false);
insert into users ( username, password, is_admin) values ('shmerik', '123456789',false);




package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;


import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.WasdappEntryRepository;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.WasdappServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.Serializable;

import static com.realdolmen.hbo5.wasdapp.wasdappcore.service.HelperClass.addTemplateAdmin;

@Controller
public class InsertController implements Serializable {

    @Autowired
    private WasdappService wasdappService;

    @Autowired
    private CurrentUser currentUser;

    //TODO
    //Change entries to dtos

    @GetMapping(value = "/insert")
    public String wasDappInsertPage(WasdappEntry wasdappEntry, Model model) {

        model.addAttribute("buttonText", "Insert");
        if (currentUser.getCurrentUser() == null){
            return "redirect:/index";
        }
        addTemplateAdmin(model, currentUser.getCurrentUser());
        return "insert.html";
    }

    @PostMapping(value = "/insert")
    public String wasdappEntryAdd(@Valid WasdappEntry wasdappEntry) {
        wasdappService.save(wasdappEntry);
        return "redirect:/homepage";
    }

    @GetMapping(value = "/insert/{id}")
    public String wasDappEditPage(@PathVariable Long id, Model model) {
        model.addAttribute("wasdappEntry", wasdappService.getOne(id));
        model.addAttribute("buttonText", "Edit");

        if (currentUser.getCurrentUser() == null){
            return "redirect:/index";
        }
        addTemplateAdmin(model, currentUser.getCurrentUser());
        return "insert.html";
    }


}

package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class TestUserController  {
    @Autowired
    private UserRepo userRepo;

    @RequestMapping("/findall")
    public String findAll(){
        String result = "";

        for(User user : userRepo.findAll()){
            result += user.toString() + "</br>";
        }

        return result;
    }


    @RequestMapping("/save")
        public String process(){
            userRepo.save(new User("Sedric","sqsqsqsq",false));
        userRepo.save(new User("michael","sqsqsqsq",true));
        userRepo.save(new User("Jelter","sqsqsqsq",false));
        //userRepo.save(new User('silvio', 'sivwlio',false));

            return "Done";
        }








}

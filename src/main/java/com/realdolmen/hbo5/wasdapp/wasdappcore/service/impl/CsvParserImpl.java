package com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.CsvParser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;

import static java.lang.Double.valueOf;
import static org.apache.logging.log4j.util.Strings.isBlank;

@Service
public class CsvParserImpl implements CsvParser {

    private WasdappService wasdappService;

    @Autowired
    public CsvParserImpl(WasdappService wasdappService) {
        this.wasdappService = wasdappService;
    }

    public void importCsv(InputStream inputStream) {

        new BufferedReader(new InputStreamReader(inputStream))
                .lines()
                .map(this::mapLineToEntry)
                .forEach(entry -> wasdappService.save(entry));
    }

    private WasdappEntry mapLineToEntry(String s) {
        String[] split = s.split(";");
        WasdappEntry wasdappEntry = new WasdappEntry();
        wasdappEntry.setId(getLongValue(split[0]));
        wasdappEntry.setName(getText(split[1]));
        wasdappEntry.setCity(getText(split[2]));
        wasdappEntry.setStreet(getText(split[3]));
        wasdappEntry.setNumber(getText(split[4]));
        wasdappEntry.setLat(getDoubleValue(split[5]));
        wasdappEntry.setLon(getDoubleValue(split[6]));
        wasdappEntry.setDescription(getText(split[7]));
        wasdappEntry.setPostcode(getText(split[8]));
        wasdappEntry.setWiki(getText(split[9]));
        wasdappEntry.setWeb(getText(split[10]));
        wasdappEntry.setTel(getText(split[11]));
        wasdappEntry.setEmail(getText(split[12]));
        wasdappEntry.setPerson(getText(split[13]));
        return wasdappEntry;
    }

    private String getText(String s) {
        return isBlank(s) ? null : s;
    }

    private Double getDoubleValue(String s) {
        return isBlank(s) ? null : valueOf(s);
    }

    private Long getLongValue(String s) {
        return isBlank(s) ? null : Long.valueOf(s);
    }
}
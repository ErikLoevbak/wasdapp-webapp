package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.CsvParser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
public class WasdappEntryController {

    private final CsvParser csvParser;
    private final WasdappService wasdappService;

    public WasdappEntryController(WasdappService wasdappService, CsvParser csvParser) {
        this.wasdappService = wasdappService;
        this.csvParser = csvParser;
    }

    @GetMapping(value = "/wasdappentry/search/{searchRequest}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WasdappEntryResponse> searchByNameLike(@PathVariable String searchRequest) {
        return wasdappService.findByNameContains(searchRequest);
    }

    @PostMapping(value = "/wasdappentry/insert")
    public Boolean insert(@RequestBody WasdappEntry wasdappEntry) {
        if(wasdappService.save(wasdappEntry).getName() == wasdappEntry.getName()){
            return true;
        }
        else{
            return false;
        }
    }

    @GetMapping(value = "/wasdappentry/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WasdappEntryResponse> findAll() {
        return wasdappService.findAll();
    }

    @GetMapping(value = "/wasdappentry/get/{id}")
    public WasdappEntryResponse findOne(@PathVariable Long id) {
        return wasdappService.getOne(id);
    }

    @DeleteMapping("/wasdappentry/get/{id}")
    public Boolean deleteEntry(@PathVariable Long id) {
        wasdappService.deleteById(id);
        return true;
    }

    @PostMapping(value = "/wasdappentry/upload")
    public Boolean recieveFile(@RequestBody MultipartFile file){
        try {
            csvParser.importCsv(file.getInputStream());
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}

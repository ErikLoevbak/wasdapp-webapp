package com.realdolmen.hbo5.wasdapp.wasdappcore.service;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;

import java.util.List;

public interface WasdappService {

    List<WasdappEntryResponse> findByNameContains(String nameShouldStartWith);

    WasdappEntry save(WasdappEntry entry);

    List<WasdappEntry> save(List<WasdappEntry> entry);


    List<WasdappEntryResponse> findAll();

    void deleteById(Long id);

    WasdappEntryResponse getOne(Long id);


}

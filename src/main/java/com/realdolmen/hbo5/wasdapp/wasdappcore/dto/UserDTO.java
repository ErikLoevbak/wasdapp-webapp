package com.realdolmen.hbo5.wasdapp.wasdappcore.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDTO {

    private Long id;

    private String userName;

    private String password;

    private Boolean isAdmin;

    public static UserDTOBuilder builder() {
        return new UserDTOBuilder();
    }
    public static final class UserDTOBuilder {
        private Long id;
        private String name;
        private String password;
        private Boolean isAdmin;

        private UserDTOBuilder() {
        }

        public UserDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }
        public UserDTOBuilder withPassword(String password) {
            this.password = password;
            return this;
        }
        public UserDTOBuilder withAdmin (Boolean admin) {
            this.isAdmin = admin;
            return this;
        }

        public UserDTO build() {
            UserDTO userDTO = new UserDTO();
            userDTO.userName = this.name;
            userDTO.password = this.password;
            userDTO.isAdmin = this.isAdmin;
            return userDTO;
        }
    }

    public UserDTO() {
    }

    public Long getId() {
        return id;
    }


    public String getUserName() {
        return userName;
    }


    public String getPassword() {
        return password;
    }


    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}

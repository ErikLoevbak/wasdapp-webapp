package com.realdolmen.hbo5.wasdapp.wasdappcore.domain;

import javax.persistence.*;

@Entity
@Table(name = "wasdapp_entry")
public class WasdappEntry {

    public WasdappEntry() {
    }

    public WasdappEntry(String name, String description, String street, String number, String city, String postcode, Double lat, Double lon, String wiki, String web, String tel, String email, String person) {
        this.name = name;
        this.description = description;
        this.street = street;
        this.number = number;
        this.city = city;
        this.postcode = postcode;
        this.lat = lat;
        this.lon = lon;
        this.wiki = wiki;
        this.web = web;
        this.tel = tel;
        this.email = email;
        Person = person;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(length = 2048)
    private String description;
    @Column(length = 96)
    private String street;
    @Column(length = 10)
    private String number;
    @Column(length = 86)
    private String city;
    @Column
    private String postcode;
    @Column
    private Double lat;
    @Column
    private Double lon;
    @Column
    private String wiki;
    @Column
    private String web;
    @Column
    private String tel;
    @Column
    private String email;
    @Column
    private String Person;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWiki() {
        return wiki;
    }

    public void setWiki(String wiki) {
        this.wiki = wiki;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPerson() {
        return Person;
    }

    public void setPerson(String person) {
        Person = person;
    }

    @Override
    public String toString() {
        return "WasdappEntry{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", city='" + city + '\'' +
                ", postcode='" + postcode + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", wiki='" + wiki + '\'' +
                ", web='" + web + '\'' +
                ", tel='" + tel + '\'' +
                ", email='" + email + '\'' +
                ", Person='" + Person + '\'' +
                '}';
    }
}

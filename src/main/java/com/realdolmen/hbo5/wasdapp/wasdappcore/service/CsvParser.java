package com.realdolmen.hbo5.wasdapp.wasdappcore.service;

import java.io.InputStream;

public interface CsvParser {
    void importCsv(InputStream inputStream);

}

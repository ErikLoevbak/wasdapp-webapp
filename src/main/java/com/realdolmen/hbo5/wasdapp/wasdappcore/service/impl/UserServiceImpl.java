package com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.UserRepo;
import com.realdolmen.hbo5.wasdapp.wasdappcore.rest.UserDtoMapper;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;


    @Override
    public Boolean save(UserDTO entry) {

        if (userRepo.findByUserNameEquals(entry.getUserName()) != null) {
            return false;
        } else {
            userRepo.save(new UserDtoMapper().dtoToUser(entry));
            return true;
        }

    }


    @Override
    public UserDTO checkUserLogin(UserDTO entry) {
        UserDTO dto = new UserDTO();

        User user = userRepo.findByUserNameEquals(entry.getUserName());
        if (user != null) {
            if (entry.getPassword().equals(user.getPassword())) {
                dto = new UserDtoMapper().UserToDto(user);
                return dto;
            } else {
                dto.setUserName("false");
                return dto;
            }
        }
        dto.setUserName("false");
        return dto;
    }


}

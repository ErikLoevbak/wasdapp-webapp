package com.realdolmen.hbo5.wasdapp.wasdappcore.service;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;

public interface UserService {

    Boolean save(UserDTO entry);

    UserDTO checkUserLogin(UserDTO entry);
}

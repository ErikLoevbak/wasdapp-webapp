package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import org.springframework.beans.factory.annotation.Autowired;

public class WasdappEntryMapper {


    public static WasdappEntryResponse mapToDto(WasdappEntry entry) {
        return WasdappEntryResponse.builder()
                .setId(entry.getId())
                .setDescription(entry.getDescription())
                .setName(entry.getName())
                .setStreet(entry.getStreet())
                .setNumber(entry.getNumber())
                .setCity(entry.getCity())
                .setPostcode(entry.getPostcode())
                .setLat(entry.getLat())
                .setLon(entry.getLon())
                .setWiki(entry.getWiki())
                .setWeb(entry.getWeb())
                .setTel(entry.getTel())
                .setEmail(entry.getEmail())
                .build();
    }

}

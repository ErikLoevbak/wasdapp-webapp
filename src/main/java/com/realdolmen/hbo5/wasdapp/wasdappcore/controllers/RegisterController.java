package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.io.Serializable;

import static com.realdolmen.hbo5.wasdapp.wasdappcore.service.HelperClass.addTemplateAdmin;

@Controller
public class RegisterController implements Serializable {

    @Autowired
    private CurrentUser currentUser;
    @Autowired
    private UserService userService;


    @GetMapping(value = "/register")
    public String wasDappRegisterPage(UserDTO userDTO, Model model) {
        if (currentUser.getCurrentUser() == null) {
            return "redirect:/index";
        } else if (currentUser.getCurrentUser().getAdmin().equals(false)) {
            return "redirect:/homepage";
        }
        addTemplateAdmin(model, currentUser.getCurrentUser());
        model.addAttribute("exist","false");
        return "register.html";
    }

    @PostMapping(value = "/register")
    public String registerUser(@Valid UserDTO userDTO, Model model) {
        if(userService.save(userDTO)){
            addTemplateAdmin(model, currentUser.getCurrentUser());
            return "redirect:/homepage";
        }
        else{
            model.addAttribute("exist", "true");
            addTemplateAdmin(model, currentUser.getCurrentUser());
            return "register.html";

        }


    }

}

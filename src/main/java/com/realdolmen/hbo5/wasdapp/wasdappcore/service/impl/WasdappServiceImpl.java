package com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import com.realdolmen.hbo5.wasdapp.wasdappcore.repo.WasdappEntryRepository;
import com.realdolmen.hbo5.wasdapp.wasdappcore.rest.WasdappEntryMapper;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class WasdappServiceImpl implements WasdappService {

    @Autowired
    private WasdappEntryRepository wasdappRepository;

    public WasdappServiceImpl(WasdappEntryRepository wasdappRepository) {
        this.wasdappRepository = wasdappRepository;
    }


    @Override
    public List<WasdappEntryResponse> findByNameContains(String shouldContain) {
        return wasdappRepository.findByNameContainingOrCityContainingAllIgnoreCase(shouldContain, shouldContain).stream()
                .map(WasdappEntryMapper::mapToDto)
                .collect(toList());
    }

    @Override
    public WasdappEntry save(WasdappEntry entry) {
        return wasdappRepository.save(entry);
    }

    @Override
    public List<WasdappEntry> save(List<WasdappEntry> entries) {
        return entries
                .stream()
                .map(this::save)
                .collect(toList());
    }

    @Override
    public List<WasdappEntryResponse> findAll() {
        return wasdappRepository.findAll().stream()
                .map(WasdappEntryMapper::mapToDto)
                .collect(toList());
    }

    @Override
    public void deleteById(Long id) {
        wasdappRepository.deleteById(id);
    }

    @Override
    public WasdappEntryResponse getOne(Long id) {
        return WasdappEntryMapper.mapToDto(wasdappRepository.getOne(id));
    }
}

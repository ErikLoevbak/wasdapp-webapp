package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;


import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

import static com.realdolmen.hbo5.wasdapp.wasdappcore.service.HelperClass.addTemplateAdmin;

@Controller
public class UserController implements Serializable {

    @Autowired
    private UserService userService;

    @Autowired
    CurrentUser currentUser;

    @GetMapping(value = "/")
    public String indexRedirect(){
        return "redirect:/index";
    }


      @GetMapping(value = "/index")
      public String loginPage(UserDTO userDTO, Model model) {
          model.addAttribute("userDTO", userDTO);
          return "index.html";
      }


    @PostMapping(value = "/index")
    public String loginUser(@Valid UserDTO userDTO) {
        UserDTO checkedUserDTO = userService.checkUserLogin(userDTO);

        if ( checkedUserDTO.getUserName() != ("false")) {
            currentUser.setCurrentUser(checkedUserDTO);
            return "redirect:/homepage";
        } else {
            return "index.html";
        }

    }

    @PostMapping(value = "/logout")
    public String logoutUser() {
            currentUser.setCurrentUser(null);
            return "redirect:/index";
        }


    }




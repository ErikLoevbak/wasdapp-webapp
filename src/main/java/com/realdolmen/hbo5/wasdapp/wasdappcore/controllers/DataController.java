package com.realdolmen.hbo5.wasdapp.wasdappcore.controllers;


import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.WasdappEntry;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.WasdappEntryResponse;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.WasdappService;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.CurrentUser;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.impl.GeneratePdfReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.ManagedBean;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import static com.realdolmen.hbo5.wasdapp.wasdappcore.service.HelperClass.addTemplateAdmin;

@ManagedBean
@Controller
public class DataController implements Serializable {

    @Autowired
    private WasdappService wasdappService;

    @Autowired
    private CurrentUser currentUser;


    @GetMapping(value = "/homepage")
    public String navigateToHomePage(Model model) {
        model.addAttribute("index", wasdappService.findAll());
        if (currentUser.getCurrentUser() == null) {
            return "redirect:/index";
        }
        addTemplateAdmin(model, currentUser.getCurrentUser());
        return "homepage.html";
    }

    @RequestMapping(value = "/pdfreport", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> citiesReport() throws IOException {

        List<WasdappEntryResponse> producten =  wasdappService.findAll();

        ByteArrayInputStream bis = GeneratePdfReport.productReport(producten);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Productsreport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    /*@RequestMapping(value="/products/download", method=RequestMethod.GET)
    @ResponseBody
    public FileSystemResource downloadFile(@Param(value="id") Long id) {
        WasdappEntry ws = wasdappService.getOne(id);
        return new FileSystemResource(new File(ws.));
    }*/

    @GetMapping(value = "/deleteMultiple")
    public String deleteMultiple(@RequestParam("id") List<Long> checkedIds, Model model) {
        if (currentUser.getCurrentUser() == null) {
            return "redirect:/index";
        }
        for (Long l : checkedIds) {
            if (l >= 0) {
                wasdappService.deleteById(l);
            }
        }
        addTemplateAdmin(model, currentUser.getCurrentUser());
        return "redirect:/homepage";
    }


}

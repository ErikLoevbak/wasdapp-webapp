package com.realdolmen.hbo5.wasdapp.wasdappcore.dto;

public class WasdappEntryResponse {

    private Long id;
    private String name;
    private String description;
    private String street, number, city, postcode;
    private Double lat, lon;
    private String wiki, web, tel, email;


    public static WasdappEntryResponseBuilder builder() {
        return new WasdappEntryResponseBuilder();
    }

    public static final class WasdappEntryResponseBuilder {
        private Long id;
        private String name;
        private String description;
        private String street, number, city, postcode;
        private Double lat, lon;
        private String wiki, web, tel, email;

        private WasdappEntryResponseBuilder() {
        }

        public WasdappEntryResponseBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public WasdappEntryResponseBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public WasdappEntryResponseBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public WasdappEntryResponseBuilder setStreet(String street) {
            this.street = street;
            return this;
        }

        public WasdappEntryResponseBuilder setNumber(String number) {
            this.number = number;
            return this;
        }

        public WasdappEntryResponseBuilder setCity(String city) {
            this.city = city;
            return this;
        }

        public WasdappEntryResponseBuilder setPostcode(String postcode) {
            this.postcode = postcode;
            return this;
        }

        public WasdappEntryResponseBuilder setLat(Double lat) {
            this.lat = lat;
            return this;
        }

        public WasdappEntryResponseBuilder setLon(Double lon) {
            this.lon = lon;
            return this;
        }

        public WasdappEntryResponseBuilder setWiki(String wiki) {
            this.wiki = wiki;
            return this;
        }

        public WasdappEntryResponseBuilder setWeb(String web) {
            this.web = web;
            return this;
        }

        public WasdappEntryResponseBuilder setTel(String tel) {
            this.tel = tel;
            return this;
        }

        public WasdappEntryResponseBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public WasdappEntryResponse build() {
            WasdappEntryResponse wasdappEntryResponse = new WasdappEntryResponse();
            wasdappEntryResponse.description = this.description;
            wasdappEntryResponse.name = this.name;
            wasdappEntryResponse.id = this.id;
            wasdappEntryResponse.street = this.street;
            wasdappEntryResponse.number = this.number;
            wasdappEntryResponse.city = this.city;
            wasdappEntryResponse.postcode = this.postcode;
            wasdappEntryResponse.lat = this.lat;
            wasdappEntryResponse.lon = this.lon;
            wasdappEntryResponse.wiki = this.wiki;
            wasdappEntryResponse.web = this.web;
            wasdappEntryResponse.tel = this.tel;
            wasdappEntryResponse.email = this.email;
            return wasdappEntryResponse;
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getWiki() {
        return wiki;
    }

    public String getWeb() {
        return web;
    }

    public String getTel() {
        return tel;
    }

    public String getEmail() {
        return email;
    }
}

package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import com.realdolmen.hbo5.wasdapp.wasdappcore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRestController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/user/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO checkLogin(@RequestBody UserDTO userDTO) {
        UserDTO response = userService.checkUserLogin(userDTO);
        response.setPassword(null);
        return response;
    }

    @PostMapping(value = "/user/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean register(@RequestBody UserDTO userDTO) {
        return userService.save(userDTO);
    }
}

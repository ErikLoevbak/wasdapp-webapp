package com.realdolmen.hbo5.wasdapp.wasdappcore.rest;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDtoMapper {


    public User dtoToUser(UserDTO userDTO){
      User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setPassword(userDTO.getPassword());
        user.setAdmin(userDTO.getAdmin());
        return  user;
    }
    public UserDTO UserToDto(User User){
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName(User.getUserName());
        userDTO.setPassword(User.getPassword());
        userDTO.setAdmin(User.getAdmin());
        return  userDTO;

    }
}

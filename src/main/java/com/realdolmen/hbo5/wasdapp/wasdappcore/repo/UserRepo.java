package com.realdolmen.hbo5.wasdapp.wasdappcore.repo;

import com.realdolmen.hbo5.wasdapp.wasdappcore.domain.User;
import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepo extends JpaRepository<User, Long>{

    User findUserByUserNameAndPassword(String username,String passw);

    User findByUserNameEquals(String userName);

    Boolean findUserByUserName(String userName);




}

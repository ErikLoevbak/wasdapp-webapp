package com.realdolmen.hbo5.wasdapp.wasdappcore.service;

import com.realdolmen.hbo5.wasdapp.wasdappcore.dto.UserDTO;
import org.springframework.ui.Model;

public class HelperClass {
    public static void addTemplateAdmin(Model model, UserDTO userDTO){
        model.addAttribute("currentUser", userDTO.getAdmin());
        model.addAttribute("LoggedIn", (userDTO != null));
        model.addAttribute("user", userDTO.getUserName());
    }
}

package com.realdolmen.hbo5.wasdapp.wasdappcore;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WasdappCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(WasdappCoreApplication.class, args);
    }


}


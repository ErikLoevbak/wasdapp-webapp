README WasDappGroupB

Configuratie server
------------------
Geen specifieke configuratie van de server nodig, Spring doet dit automatisch waardoor de gebruiker de applicatie kan draaien.

Project openen
---------------
1. Installeer IntelliJ
2. Download en unzip het zip-bestand
3. Ga in IntelliJ naar File, selecteer Open... en open de unzipte map
4. Zorg ervoor dat WasdappCoreApplication geselecteerd is, zodat het project kan gerund worden. 
5. Klik dan op de runknop naast de WasdappCoreApplication om het project op te starten. 
6. Ga naar localhost:8080/


Functionaliteiten WEEK1(18-22 februari 2019)
--------------------------------------------

index.html
-----------
Op deze pagina kan de gebruiker zich inloggen met zijn/haar inloggegevens. Daarna krijgt de gebruiker de homepagina te zien. De andere mogelijkheid voor de gebruiker is dat hij/zij 
zich kan registreren. Deze gegevens worden op dit moment nog nergens opgeslagen of weggeschreven naar de databank. Deze extra functie wordt volgende week toegevoegd.


homepage.html
------------
De homepagina geeft alle gegevens weer die in de databank staan opgeslagen. De gebruiker heeft op deze pagina de mogelijkheid om een rij met gegevens toe te voegen, te verwijderen 
of te wijzigen.

insert.html
------------
De gebruiker voegt op deze pagina gegevens toe, die daarna in de tabel op de homepagina worden weergegeven. 

register.html
--------------
Op deze pagina registreert de gebruiker zich, deze gegevens worden later nog weggeschreven naar de databank. 
